package com.isoft.intern;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author Stefan Ivanov
 */
public class Validator {


    /**
     * Tries to Integer.parseInt
     *
     * @param text string to try parse as integer
     * @return if string is valid Integer
     */
    public static boolean isInteger(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Inadequate method
     *
     * @param text text to be checked if is int
     * @return true if String param is valid Integer
     */
    @Deprecated
    public static boolean isNumber(String text) {
        return text.matches("\\d+");
    }

    /**
     * Tries to parse text as double
     *
     * @param text the text to be checked
     * @return true if its valid Double, false if exception occurs
     */
    public static boolean isDouble(String text) {
        if (text.isEmpty())
            return false;
        try {
            Double.parseDouble(text);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Check if number is between 2 values
     *
     * @param number       number to be checked
     * @param lowerBoundry min value
     * @param upperBoundry max valu
     * @return if number is higher than or equals min and lower than or equals max (<= >=)
     */
    public static boolean isNumberBetween(int number, int lowerBoundry, int upperBoundry) {
        return number >= lowerBoundry && number <= upperBoundry;
    }


    /**
     * Uses regex to check if phone number is in valid format
     * +359 or 0 followed by 9 digits
     *
     * @param phoneNumber the string representation of the phone number to be checked
     * @return true if valid format
     */
    public static boolean isValidPhoneNumber(String phoneNumber) {
        String regex = "^(0|\\+359)[0-9]{9}$";
        return Pattern.compile(regex).matcher(phoneNumber).matches();
    }


    /**
     * Tries to parse the string representation of date, by a given format
     *
     * @param date   the String representation of the date
     * @param format the format of which the date should entered
     * @return true if date is valid format
     */
    public static boolean isValidDate(String date, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);

        try {
            formatter.parse(date);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }

    /**
     * Checks if the given date is in the past
     *
     * @param date the Date to be checked
     * @return true if date is in the past
     */
    public static boolean isDateInThePast(Date date) {
        Date now = new Date();

        return date.before(now);
    }
}
