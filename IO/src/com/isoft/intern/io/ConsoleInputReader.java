package com.isoft.intern.io;

import com.isoft.intern.io.interfaces.InputReader;

import java.util.Scanner;

/**
 * @author Stefan Ivanov
 */
public class ConsoleInputReader implements InputReader {
    private Scanner scanner;

    public ConsoleInputReader() {
        scanner = new Scanner(System.in);
    }

    public String readLine() {
        return scanner.nextLine();
    }

}
