package com.isoft.intern.io.interfaces;

/**
 * @author Stefan Ivanov
 */
public interface InputReader {

    String readLine();
}
