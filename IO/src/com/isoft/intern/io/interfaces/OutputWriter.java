package com.isoft.intern.io.interfaces;

/**
 * @author Stefan Ivanov
 */
public interface OutputWriter {

    void write(String data);
}
