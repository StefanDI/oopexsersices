package com.isoft.intern.exsersice3;

import com.isoft.intern.exsersice3.menu.CalculatorMenu;
import com.isoft.intern.io.ConsoleInputReader;
import com.isoft.intern.io.ConsoleOutputWriter;

/**
 * @author Stefan Ivanov
 */
public class Main {
    public static void main(String[] args) {
        CalculatorMenu calculatorMenu =
                new CalculatorMenu(new ConsoleInputReader(), new ConsoleOutputWriter());

        calculatorMenu.start();

    }

}