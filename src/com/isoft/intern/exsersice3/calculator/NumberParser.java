package com.isoft.intern.exsersice3.calculator;

import com.isoft.intern.Validator;

/**
 * @author Stefan Ivanov
 */
public class NumberParser {

    private NumberParser() {
    }

    /**
     * Returns the proper Number from String
     *
     * @param number the string to parse
     * @return string parsed as {@link Number}
     * @throws IllegalArgumentException If invalid string input
     */
    public static Double parseDouble(String number) throws IllegalArgumentException {
        if (Validator.isDouble(number))
            return Double.valueOf(number);

        throw new IllegalArgumentException("Wrong number format");
    }
}
