package com.isoft.intern.exsersice3.calculator;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author Stefan Ivanov
 */
public class Calculator {
    //private constructor so it cants be instantiated
    private Calculator() {
    }

    public static Double calculate(Integer a, Integer b,
                                   BiFunction<Integer, Integer, Double> operation) {
        return operation.apply(a, b);
    }

    public static Double calculate(Double a, Double b,
                                   BiFunction<Double, Double, Double> operation) {
        return operation.apply(a, b);
    }

    public static Double calculate(Double a, Integer b,
                                   BiFunction<Double, Integer, Double> operation) {
        return operation.apply(a, b);
    }

    public static Double calculate(Double a, Function<Double, Double> operation) {
        return operation.apply(a);
    }

    public static Double calculate(Integer a, Function<Integer, Double> operation) {
        return operation.apply(a);
    }

}

