package com.isoft.intern.exsersice3.calculator.parsers;

/**
 * @author Stefan Ivanov
 */
public class TrigonometricParser {

    /**
     * Extracts trigonometric function from string
     *
     * @param input the string
     * @return the proper trigonometric function
     * @throws IllegalArgumentException if no function is supplied or unknown function
     */
    public static TrigonometricFunction getFunction(String input) throws IllegalArgumentException {

        String[] tokens = input.split("\\(");
        String function = tokens[0];
        for (TrigonometricFunction value : TrigonometricFunction.values()) {
            if (value.name().toLowerCase().equals(
                    function.toLowerCase())) {
                return value;
            }
        }
        throw new IllegalArgumentException("Unknown trigonometric function");
    }

    /**
     * Extracts the value between parenthesis
     * it doesnt care if the value is number or not
     *
     * @param input the string
     * @return the value inside the ( )
     */
    public static String extractValue(String input) {
        if (input.contains("(") && input.contains(")"))
            return input.substring(
                    input.indexOf("(") + 1,
                    input.indexOf(")"));

        throw new IllegalArgumentException("Wrong input format\nAre you sure you closed parenthesis?");
    }
}
