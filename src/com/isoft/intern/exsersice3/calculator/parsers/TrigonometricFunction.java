package com.isoft.intern.exsersice3.calculator.parsers;

import java.util.function.Function;

/**
 * @author Stefan Ivanov
 */
public enum TrigonometricFunction {
    SIN(Math::sin),
    COS(Math::cos),
    TG(Math::tan),
    COTG((x) -> 1 / Math.tan(x)),
    ARCSIN(Math::asin),
    ARCOS(Math::acos);

    public final Function<Double, Double> function;

    TrigonometricFunction(Function<Double, Double> function) {
        this.function = function;
    }
}
