package com.isoft.intern.exsersice3.calculator.parsers;

/**
 * @author Stefan Ivanov
 */
public class ArithmeticParser {

    /**
     * Parses function from string input
     *
     * @param input the string input
     * @return the ArithmeticOperation
     * @throws IllegalArgumentException if unknown operation is supplied
     */
    public static ArithmeticFunction getFunction(String input) throws IllegalArgumentException {
        //Example format  5 + 4 || 32.3 * 8
        String[] tokens = input.split(" ");
        String function = tokens[1];
        for (ArithmeticFunction value : ArithmeticFunction.values()) {
            if (value.getFunctionAsString().toLowerCase().equals(
                    function.toLowerCase())) {
                return value;
            }
        }

        throw new IllegalArgumentException("Unknown arithmetic function");
    }

    public static String[] extractValues(String input) {
        String[] tokens = input.split(" ");

        return new String[]{tokens[0], tokens[2]};
    }
}
