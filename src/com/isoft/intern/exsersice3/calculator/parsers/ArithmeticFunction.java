package com.isoft.intern.exsersice3.calculator.parsers;

import java.util.function.BiFunction;

/**
 * @author Stefan Ivanov
 */
public enum ArithmeticFunction {
    SUM(Double::sum, "+"),
    SUBTRACT((a, b) -> a - b, "-"),
    MULTIPLY((a, b) -> a * b, "*"),
    DIVIDE((a, b) -> a / b, "/");

    public final BiFunction<Double, Double, Double> function;

    public final String functionAsString;

    ArithmeticFunction(BiFunction<Double, Double, Double> function, String functionAsString) {
        this.function = function;
        this.functionAsString = functionAsString;
    }

    public String getFunctionAsString() {
        return functionAsString;
    }
}
