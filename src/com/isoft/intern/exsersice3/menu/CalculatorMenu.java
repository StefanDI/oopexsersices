package com.isoft.intern.exsersice3.menu;

import com.isoft.intern.exsersice3.calculator.Calculator;
import com.isoft.intern.exsersice3.calculator.NumberParser;
import com.isoft.intern.exsersice3.calculator.parsers.ArithmeticFunction;
import com.isoft.intern.exsersice3.calculator.parsers.ArithmeticParser;
import com.isoft.intern.exsersice3.calculator.parsers.TrigonometricFunction;
import com.isoft.intern.exsersice3.calculator.parsers.TrigonometricParser;
import com.isoft.intern.io.interfaces.InputReader;
import com.isoft.intern.io.interfaces.OutputWriter;

/**
 * @author Stefan Ivanov
 */
public class CalculatorMenu {

    private InputReader inputReader;
    private OutputWriter outputWriter;

    public CalculatorMenu(InputReader inputReader, OutputWriter outputWriter) {
        this.inputReader = inputReader;
        this.outputWriter = outputWriter;
    }

    public void start() {
        showMainMenu();
    }

    private void showMainMenu() {
        String menu = "Welcome to calculator\n" +
                "Please, choose an option\n" +
                "1.Arithmetic calculation\n" +
                "2.Trigonometric calculation \n" +
                "3.Exit\n";

        outputWriter.write(menu);

        handleMainMenuInput();
    }

    /**
     * Depending on the option selected
     * prints the appropriate result
     */
    private void handleMainMenuInput() {
        String input = inputReader.readLine();

        switch (input) {
            //Arithmetic
            case "1":
                outputWriter.write("Write an arithmetic operation (ex. 1 + 3)\n" +
                        "NOTE: separate with 1 whitespace\n");
                input = inputReader.readLine();
                try {
                    ArithmeticFunction arithmeticFunction =
                            ArithmeticParser.getFunction(input);

                    String[] values =
                            ArithmeticParser.extractValues(input);

                    Double number1 = NumberParser.parseDouble(values[0]);

                    Double number2 = NumberParser.parseDouble(values[1]);

                    Double answer = Calculator.calculate(number1, number2, arithmeticFunction.function);

                    String format = isDoubleInt(answer) ? "Answer: %.0f" : "Answer: %.1f";

                    outputWriter.write(
                            String.format(format, answer));
                } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException wrongInput) {
                    outputWriter.write(wrongInput.getMessage());
                }
                pressEnterToContinue();
                showMainMenu();
                break;
            //Trigonometric
            case "2": {
                outputWriter.write("Input trigonometric operation (ex. sin(5) )\n" +
                        "NOTE: number should be in degrees\n");
                input = inputReader.readLine();
                try {
                    TrigonometricFunction trigonometricFunction =
                            TrigonometricParser.getFunction(input);

                    String value =
                            TrigonometricParser.extractValue(input);

                    Double num = Math.toRadians(NumberParser.parseDouble(value));

                    Double answer = Calculator.calculate(num, trigonometricFunction.function);

                    String format = isDoubleInt(answer) ? "Answer: %.0f" : "Answer: %.1f";

                    outputWriter.write(
                            String.format(format, answer));

                } catch (IllegalArgumentException wrongInputException) {
                    outputWriter.write(wrongInputException.getMessage());
                }

                pressEnterToContinue();
                showMainMenu();
            }
            break;
            //Exit
            case "3":
                return;
            default:
                outputWriter.write("Invalid input\n");
                showMainMenu();
        }

    }

    /**
     * Util method to check if double is actually an integer
     * @param d the double to check
     * @return true if parameter is integer
     */
    private boolean isDoubleInt(double d) {
        //select a "tolerance range" for being an integer
        double TOLERANCE = 1E-5;
        //do not use (int)d, due to weird floating point conversions!
        return Math.abs(Math.floor(d) - d) < TOLERANCE;
    }

    /**
     * Util method to wait for any input of the user
     */
    private void pressEnterToContinue() {
        outputWriter.write("\nPress any key to continue\n");
        inputReader.readLine();
    }
}
