package com.isoft.intern.exsersice1.menu;

import com.isoft.intern.Validator;
import com.isoft.intern.exsersice1.cars.*;
import com.isoft.intern.io.interfaces.InputReader;
import com.isoft.intern.io.interfaces.OutputWriter;

import java.util.List;

/**
 * Start point of Cars App
 *
 * @author Stefan Ivanov
 */
public class CarsMenu {

    private InputReader inputReader;
    private OutputWriter outputWriter;
    private CarsHolder carsHolder;

    /**
     * @param reader in our case input from console reader
     * @param writer in our case output to console
     */
    public CarsMenu(InputReader reader, OutputWriter writer) {
        inputReader = reader;
        outputWriter = writer;
        carsHolder = new CarsHolder();
    }

    /**
     * Start point of the menu
     */
    public void start() {
        showMainMenu();
    }

    /**
     * show main menu
     */
    private void showMainMenu() {
        String menu = "Welcome to cars\n" +
                "Please select one of the following options:\n" +
                "1.Add a car\n" +
                "2.List all cars\n" +
                "3.Remove car\n";

        outputWriter.write(menu);

        handleMainMenuInput();
    }

    /**
     * Handles the input for main menu
     */
    private void handleMainMenuInput() {
        switch (inputReader.readLine()) {
            case "1":
                addCarMenu();
                break;
            case "2": {
                showListCarsMenu();
            }
            break;
            case "3":
                showRemoveCarMenu();
                break;
            default:
                outputWriter.write("Invalid input\n");
                showMainMenu();
                break;
        }
    }

    /**
     * Outputs all cars
     */
    private void showListCarsMenu() {
        String menu = extractAllCarList();
        menu += "Press enter to go back to main menu\n";
        outputWriter.write(menu);
        handleListCarsMenuInput();
    }

    /**
     * Waits for random input of the user, to show main menu
     */
    private void handleListCarsMenuInput() {
        inputReader.readLine();
        showMainMenu();
    }

    /**
     * Show all cars and wait for valid input
     * of index of car to remove
     * Validates input for {@link #handleRemoveCarInput(int)} method
     */
    private void showRemoveCarMenu() {
        String menu = "Select car index to remove";
        menu += extractAllCarList();
        menu += String.format("%d. Back%n", carsHolder.getCars().size());
        outputWriter.write(menu);
        String input;
        int intInput;
        //Only break loop if input is valid
        while (true) {
            input = inputReader.readLine();
            if (Validator.isInteger(input)) {
                intInput = Integer.valueOf(input);
                if (Validator.isNumberBetween(intInput, 0, carsHolder.getCars().size())) {
                    break;
                }
            }

            outputWriter.write("Invalid input\n");
        }
        handleRemoveCarInput(intInput);

    }

    /**
     * Removes the car at the desired index
     * It doesnt do validation, needs valid parameter
     *
     * @param intInput Valid intInput representing the option of the menu chosen
     */
    private void handleRemoveCarInput(int intInput) {
        //This is the back button
        if (intInput == carsHolder.getCars().size()) {
            showMainMenu();
        } else {
            outputWriter.write("Successfully removed car");
            carsHolder.removeCar(intInput);
            showRemoveCarMenu();
        }
    }

    /**
     * Show car create menu until valid input is entered
     */
    private void addCarMenu() {
        String yearInput;
        String horsePowerInput;
        String brand;
        while (true) {
            outputWriter.write("Please select year of manufacture, between 1900 and 2020:\n");
            yearInput = inputReader.readLine();

            outputWriter.write("Please select horse power between 50 and 200:\n");
            horsePowerInput = inputReader.readLine();

            outputWriter.write("Select the car brand:\n" +
                    "1.BMW\n" +
                    "2.Peugeout\n" +
                    "3.Renault\n" +
                    "4.Wolkswagen\n");
            brand = inputReader.readLine();

            //If the input is valid, break the loop
            if (validateNewCarInput(yearInput, horsePowerInput, brand)) {
                break;
            }

            outputWriter.write("Invalid input\n");

        }


        handleAddCarInput(yearInput, horsePowerInput, brand);
    }

    /**
     * Parses the input and adds car to the carHolder
     *
     * @param yearInput       String representation of yearOfManufacture
     * @param horsePowerInput String representation of horsePower
     * @param brandIndex      String representation of the brand index (of the menu)
     */
    private void handleAddCarInput(String yearInput, String horsePowerInput, String brandIndex) {
        String className = extractClassNameFromBrand(brandIndex);
        int yearOfManufacturer = Integer.parseInt(yearInput);
        int horsePower = Integer.parseInt(horsePowerInput);

        //Build a car
        Car car = CarBuilder.buildCar(yearOfManufacturer, horsePower, className);
        //Add it to the carsHolder
        carsHolder.addCar(car);

        //Show success message and redirect to main menu
        outputWriter.write("Successfully added car\n");
        showMainMenu();
    }

    /**
     * Util method to extract String representation
     * of list of all cars indexed
     *
     * @return the extracted String
     */
    private String extractAllCarList() {
        List<Car> cars = carsHolder.getCars();
        //If empty list
        if (cars.size() == 0) {
            return "\nNo cars\n";
        }
        StringBuilder allCarsList = new StringBuilder("\n");

        for (int i = 0; i < cars.size(); i++) {
            allCarsList.append(
                    String.format("%d. %s%n", i, cars.get(i).toString()));
        }

        return allCarsList.toString();
    }

    /**
     * Validate the year of manufacture, horse power and brand inputs
     *
     * @param yearInput       year to be validated
     * @param horsePowerInput horse power to be validated
     * @param brand           brand to be validated
     * @return true if all inputs are valid
     */
    private boolean validateNewCarInput(String yearInput, String horsePowerInput, String brand) {
        //First validate if they are integers
        if (Validator.isInteger(yearInput)
                && Validator.isInteger(horsePowerInput)
                && Validator.isInteger(brand)) {
            int yearOfManufacture = Integer.valueOf(yearInput);
            int horsePower = Integer.valueOf(horsePowerInput);
            int carBrand = Integer.valueOf(brand);

            //Then validate constrains
            return Validator.isNumberBetween(yearOfManufacture, 1900, 2020)
                    && Validator.isNumberBetween(horsePower, 50, 200)
                    && Validator.isNumberBetween(carBrand, 1, 4);
        }
        //If not valid integer, return false
        return false;
    }


    /**
     * Return class name associated with brand menu option
     * needed for CarBuilder
     *
     * @param brandIndex the index of the menu
     * @return the class name of the corresponding brand
     */
    private String extractClassNameFromBrand(String brandIndex) {
        String brandName = "";
        switch (brandIndex) {
            case "1":
                brandName = BMW.class.getName();
                break;
            case "2":
                brandName = Peugeot.class.getName();
                break;
            case "3":
                brandName = Renault.class.getName();
                break;
            case "4":
                brandName = Wolkswagen.class.getName();
                break;
        }
        return brandName;
    }

}
