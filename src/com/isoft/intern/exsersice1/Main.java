package com.isoft.intern.exsersice1;

import com.isoft.intern.exsersice1.menu.CarsMenu;
import com.isoft.intern.io.ConsoleInputReader;
import com.isoft.intern.io.ConsoleOutputWriter;

/**
 * @author Stefan Ivanov
 */
public class Main {
    public static void main(String[] args) {
        CarsMenu carsMenu = new CarsMenu(new ConsoleInputReader(), new ConsoleOutputWriter());

        carsMenu.start();
    }
}
