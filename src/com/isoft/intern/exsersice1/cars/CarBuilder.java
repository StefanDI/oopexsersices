package com.isoft.intern.exsersice1.cars;

/**
 * @author Stefan Ivanov
 */
public class CarBuilder {
    private CarBuilder() {
    }

    /**
     * Builds a car implementation for with className and constructor parameters
     * Will get valid data
     *
     * @param yearOfManufacture year of manufacture of the car to be build
     * @param horsePower        horse power of the car to be build
     * @param className         class name of the car to be build (for reflection)
     * @return new appropriate Car implementation null if any exception occurs
     */
    public static Car buildCar(int yearOfManufacture, int horsePower, String className) {
        try {

            return (Car) Class.forName(className)
                    .getConstructor(int.class, int.class)
                    .newInstance(horsePower, yearOfManufacture);
        } catch (Exception exception) {
            return null;}

    }
}
