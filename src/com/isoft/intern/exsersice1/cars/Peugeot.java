package com.isoft.intern.exsersice1.cars;

/**
 * @author Stefan Ivanov
 */
public class Peugeot extends Car {
    private static final String BRAND = "Peugeot";

    public Peugeot(int horsePower, int yearOfManufacture) {
        super(horsePower, yearOfManufacture, BRAND);
    }
}
