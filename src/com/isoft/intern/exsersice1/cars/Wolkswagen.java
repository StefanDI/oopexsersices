package com.isoft.intern.exsersice1.cars;

/**
 * subclass of Car abstract class
 *
 * @author Stefan Ivanov
 */
public class Wolkswagen extends Car {

    private static final String BRAND = "Wolkswagen";

    public Wolkswagen(int horsePower, int yearOfManufacture) {
        super(horsePower, yearOfManufacture, BRAND);
    }
}
