package com.isoft.intern.exsersice1.cars;


/**
 * Abstract Car class
 *
 * @author Stefan Ivanov
 */
public abstract class Car {

    private int horsePower;
    private int yearOfManufacture;
    private String brand;

    /**
     * @param horsePower        horse power of the car
     * @param yearOfManufacture year of creation
     * @param brand             car specific brand
     */
    protected Car(int horsePower, int yearOfManufacture, String brand) {
        this.horsePower = horsePower;
        this.yearOfManufacture = yearOfManufacture;
        this.brand = brand;
    }


    //Getters and setters
    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    //Override toString

    @Override
    public String toString() {
        return "Car{" +
                "horsePower=" + horsePower +
                ", yearOfManufacture=" + yearOfManufacture +
                ", brand='" + brand + '\'' +
                '}';
    }
}
