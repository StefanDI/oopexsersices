package com.isoft.intern.exsersice1.cars;

/**
 * @author Stefan Ivanov
 */
public class Renault extends Car {
    private static final String BRAND = "Renault";

    public Renault(int horsePower, int yearOfManufacture) {
        super(horsePower, yearOfManufacture, BRAND);
    }
}
