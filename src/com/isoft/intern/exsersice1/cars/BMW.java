package com.isoft.intern.exsersice1.cars;


/**
 * subclass of Car abstract class
 *
 * @author Stefan Ivanov
 */
public class BMW extends Car {

    private static String BRAND = "BMW";

    public BMW(int horsePower, int yearOfManufacture) {
        super(horsePower, yearOfManufacture, BRAND);
    }

}
