package com.isoft.intern.exsersice1.cars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Stefan Ivanov
 */
public class CarsHolder {
    private List<Car> cars;

    public CarsHolder() {
        cars = new ArrayList<>();
    }

    public void addCar(Car c) {
        cars.add(c);
    }

    public void removeCar(Car c) {
        cars.remove(c);
    }

    public void removeCar(int index) {
        cars.remove(index);
    }

    public List<Car> getCars() {
        return Collections.unmodifiableList(cars);
    }
}
