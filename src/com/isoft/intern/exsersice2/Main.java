package com.isoft.intern.exsersice2;

import com.isoft.intern.exsersice2.menu.CalendarMenu;
import com.isoft.intern.io.ConsoleInputReader;
import com.isoft.intern.io.ConsoleOutputWriter;

/**
 * @author Stefan Ivanov
 */
public class Main {
    public static void main(String[] args) {
        CalendarMenu menu = new CalendarMenu(new ConsoleInputReader(), new ConsoleOutputWriter());
        menu.start();
    }
}
