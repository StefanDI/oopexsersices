package com.isoft.intern.exsersice2.menu;

import com.isoft.intern.Validator;
import com.isoft.intern.exsersice2.calendar.EventCalendar;
import com.isoft.intern.io.interfaces.InputReader;
import com.isoft.intern.io.interfaces.OutputWriter;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
public class CalendarMenu {

    private InputReader inputReader;
    private OutputWriter outputWriter;
    private EventCalendar eventCalendar;

    public CalendarMenu(InputReader inputReader, OutputWriter outputWriter) {
        this.inputReader = inputReader;
        this.outputWriter = outputWriter;
        eventCalendar = new EventCalendar();
    }

    public void start() {
        showMainMenu();
    }

    private void showMainMenu() {
        String menu = "Welcome to event calender:\n" +
                "Please, select an option\n" +
                "1.Add an event\n" +
                "2.Show all events\n" +
                "3.Exit\n";

        outputWriter.write(menu);

        handleMainMenuInput();
    }

    /**
     * Handles main menu input
     */
    private void handleMainMenuInput() {
        String input = inputReader.readLine();

        switch (input) {
            case "1": {
                addEventMenu();
            }
            break;
            case "2": {
                allEventsMenu();
            }
            break;
            case "3":
                return;
            default:
                outputWriter.write("Invalid input\n");
                showMainMenu();
                break;
        }
    }

    /**
     * Outputs all events
     */
    private void allEventsMenu() {
        List<String> allEvents = eventCalendar.getEvents();

        if (allEvents.isEmpty())
            outputWriter.write("No events\n");
        else {
            for (int i = 0; i < allEvents.size(); i++) {
                outputWriter.write(
                        String.format("%d. %s%n",
                                i, allEvents.get(i)));
            }
        }

        outputWriter.write(String.format("%d. Back%n",
                allEvents.size()));

        outputWriter.write("Select event for more options\n");

        handleEventInput();
    }

    /**
     * handles and validates input for allEvents menu
     */
    private void handleEventInput() {
        String input = inputReader.readLine();
        if (Validator.isNumber(input) &&
                Validator.isNumberBetween(
                        Integer.valueOf(input), 0, eventCalendar.getEvents().size())) {
            //Back option
            if (Integer.valueOf(input) == eventCalendar.getEvents().size()) {
                showMainMenu();
                return;
            }

            //If it is valid and is not back option, show the event and additional options
            showSingleEvent(Integer.valueOf(input));
        } else {
            outputWriter.write("Invalid input\nSelect another option\n");
            handleEventInput();
        }
    }

    /**
     * shows more options for specific event
     *
     * @param indexOfEvent the index of the event to be shown
     */
    private void showSingleEvent(int indexOfEvent) {
        outputWriter.write(eventCalendar.getEventAt(indexOfEvent));
        String menu = "\nSelect an option\n" +
                "1.Remove event\n" +
                "2.See remaining time\n" +
                "3.Back\n";

        outputWriter.write(menu);

        handleSingleEventInput(indexOfEvent);
    }

    /**
     * handles the input for single event (remove or show remaining time)
     *
     * @param eventIndex the index of the event from the List
     */
    private void handleSingleEventInput(int eventIndex) {
        String input = inputReader.readLine();

        switch (input) {
            case "1":
                eventCalendar.removeEventByIndex(eventIndex);
                outputWriter.write("Successfully removed event\n");
                pressEnterToContinue();
                allEventsMenu();
                break;
            case "2":
                outputWriter.write(
                        String.format("Remaining time: %s%n",
                                eventCalendar.remainingTimeUntilEvent(eventIndex)));

                pressEnterToContinue();
                allEventsMenu();
                break;
            case "3":
                showMainMenu();
                break;
            default:
                outputWriter.write("Wrong input\n");
                handleSingleEventInput(eventIndex);
                break;

        }
    }

    /**
     * show add event menu and validates input
     */
    private void addEventMenu() {
        outputWriter.write("Input date of the event in this format "
                + EventCalendar.ACCEPTED_DATE_FORMAT + "\n");
        String date = inputReader.readLine();

        outputWriter.write("Input description of the event\n");
        String description = inputReader.readLine();

        try {
            eventCalendar.addEvent(date, description);
            outputWriter.write("Successfully added event\n");

            pressEnterToContinue();
            showMainMenu();
        } catch (IllegalArgumentException e) {
            //Appropriate message written in addEvent method
            outputWriter.write(e.getMessage());
            addEventMenu();
        }
    }

    /**
     * Util method to wait for any input of the user
     */
    private void pressEnterToContinue() {
        outputWriter.write("\nPress enter continue\n");
        inputReader.readLine();
    }
}
