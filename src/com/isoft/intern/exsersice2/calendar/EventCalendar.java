package com.isoft.intern.exsersice2.calendar;

import com.isoft.intern.Validator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Stefan Ivanov
 */
public class EventCalendar {

    public static final String ACCEPTED_DATE_FORMAT = "dd-MM-yyyy";

    private List<CalendarEntry> events;

    public EventCalendar() {
        events = new ArrayList<>();
    }

    /**
     * Maps all CalendarEntries to their string representations, and returns them
     *
     * @return List of all the events' String representation
     */
    public List<String> getEvents() {
        return events.stream()
                .map(Object::toString)
                .collect(Collectors.toList());
    }

    /**
     * Get the string representation of the event at a given index
     *
     * @param index index at which to fetch event
     * @return the string representation of the event
     */
    public String getEventAt(int index) {
        return events.get(index).toString();
    }

    /**
     * Removes event at the desired index
     *
     * @param index the index of the event to be removed
     */
    public void removeEventByIndex(int index) {
        events.remove(index);
    }

    /**
     * Return remaining remaining days until an event
     *
     * @param eventIndex the index of the event
     * @return number of days until event
     */
    public String remainingTimeUntilEvent(int eventIndex) {
        Date now = new Date();
        CalendarEntry entry = events.get(eventIndex);

        int days = daysBetween(now, entry.date);

        if (days > 7) {
            int weeks = days / 7;
            days = (days % 7) - 1;
            return String.format("Weeks: %s, Days: %s", weeks, days);
        }
        return String.format("Days: %s",
                days);
    }

    /**
     * Util method to calculate the difference in days between 2 dates
     *
     * @param d1 the first date
     * @param d2 the second date
     * @return difference in days between the 2 dates
     */
    private int daysBetween(Date d1, Date d2) {
        /*
          Math.abs is because the method doesnt know which date is "smaller" in terms of time
          since Date.getTime() returns the milliseconds from the start of time (1970)
          we need to divide by
          ( 1000 - to convert to seconds , 60 to convert to minutes, 60 to convert to hours, 24 to convert to days )
         */
        return Math.abs((int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24)));
    }

    /**
     * Adds event by date and description
     *
     * @param date        the string representation of the date
     * @param description description of the event
     * @throws IllegalArgumentException if the string representation of the date is invalid format
     *                                  or if date is invalid (ex: 05-20-2020)
     */
    public void addEvent(String date, String description)
            throws IllegalArgumentException {
        if (!Validator.isValidDate(date, ACCEPTED_DATE_FORMAT))
            throw new IllegalArgumentException("Invalid date\n");

        Date eventDate = stringToDate(date);

        if (Validator.isDateInThePast(eventDate))
            throw new IllegalArgumentException("Date cannot be in the past\n");

        CalendarEntry entry = new CalendarEntry(eventDate, description);

        events.add(entry);
    }

    /**
     * Returns {@link Date} object from valid string representation of date
     *
     * @param date the string representation of the date
     * @return the {@link Date} object
     */
    private Date stringToDate(String date) {
        try {
            return new SimpleDateFormat(ACCEPTED_DATE_FORMAT).parse(date);
        } catch (ParseException ignored) {
            //This method is passed only valid date
            return new Date();
        }
    }


    /**
     * private class representing single {@link EventCalendar} entry
     */
    private class CalendarEntry {
        private Date date;
        private String description;

        public CalendarEntry(Date date, String description) {
            this.date = date;
            this.description = description;
        }

        @Override
        public String toString() {
            DateFormat dateFormat = new SimpleDateFormat(ACCEPTED_DATE_FORMAT);
            return "Event: " +
                    "date=" + dateFormat.format(date) +
                    ", description='" + description + "'";
        }
    }
}
