package com.isoft.intern.exsersice4.menu;

import com.isoft.intern.Validator;
import com.isoft.intern.exsersice4.phonebook.PhoneBook;
import com.isoft.intern.io.interfaces.InputReader;
import com.isoft.intern.io.interfaces.OutputWriter;

import java.util.List;

/**
 * @author Stefan Ivanov
 */
public class Menu {
    private InputReader inputReader;
    private OutputWriter outputWriter;
    private PhoneBook phoneBook;

    public Menu(InputReader inputReader, OutputWriter outputWriter) {
        this.inputReader = inputReader;
        this.outputWriter = outputWriter;
        phoneBook = new PhoneBook();
    }

    /**
     * Start point of the app
     */
    public void start() {
        showMainMenu();
    }


    /*Main menu*/
    private void showMainMenu() {
        String menu = "Welcome to phone book app\n" +
                "Please select an option\n" +
                "1.Insert into phone book\n" +
                "2.Search the phone book\n" +
                "3.Delete from phone book\n" +
                "4.Exit\n";

        outputWriter.write(menu);

        handleMainMenuInput();
    }

    /**
     * Handles menu option and redirects to desired menu
     */
    private void handleMainMenuInput() {
        String input = inputReader.readLine();

        switch (input) {
            case "1":
                showInsertMenu();
                break;
            case "2":
                showSearchMenu();
                break;
            case "3":
                showRemoveMenu();
                break;
            case "4":
                return;
            default: {
                outputWriter.write("Invalid input\n");
                handleMainMenuInput();
            }
        }
    }

    /**
     * Reads phone number from console and handles input
     */
    private void showRemoveMenu() {
        String phoneNumber = readPhoneNumberInput();

        handleRemovePhoneEntry(phoneNumber);
    }

    /**
     * Tries to remove the phone entry associated with this phone number
     * and outputs appropriate message
     *
     * @param phoneNumber the phone number to be deleted
     */
    private void handleRemovePhoneEntry(String phoneNumber) {
        if (phoneBook.removeEntryByPhoneNumber(phoneNumber)) {
            outputWriter.write("Successfully removed phone number\n");
            pressEnterToContinue();
            showMainMenu();
        } else {
            outputWriter.write("No such phone number\n");
            pressEnterToContinue();
            showMainMenu();
        }
    }


    /**
     * Show the search menu
     */
    private void showSearchMenu() {
        String menu = "Please choose an option\n" +
                "1.Search by phone number\n" +
                "2.Search by name\n" +
                "3.Back\n";

        outputWriter.write(menu);

        handleSearchMenuInput();
    }

    /**
     * Handles input from search menu and
     * outputs desired option
     */
    private void handleSearchMenuInput() {
        String input = inputReader.readLine();

        switch (input) {
            //Search by phone number
            case "1":
                String phoneNumber = readPhoneNumberInput();

                //If null, output is no such phone number, else
                //string representation of the phone entry
                PhoneBook.PhoneEntry phoneEntry = phoneBook.getPhoneEntryByPhoneNumber(phoneNumber);
                String output =
                        phoneEntry == null ?
                                "No such phone number\n"
                                :
                                phoneEntry.toString() + "\n";

                outputWriter.write(output);
                pressEnterToContinue();
                showSearchMenu();
                break;
            //Search by name
            case "2":
                String name = inputReader.readLine();
                List<PhoneBook.PhoneEntry> allByName = phoneBook.getAllByName(name);
                if (allByName.isEmpty())
                    outputWriter.write("No entries with this name\n");
                for (PhoneBook.PhoneEntry entry : allByName) {
                    outputWriter.write(entry.toString() + "\n");
                }
                pressEnterToContinue();
                showSearchMenu();
                break;
            //Back
            case "3":
                showMainMenu();
                break;
            //Wrong input
            default:
                outputWriter.write("Invalid input\n");
                showSearchMenu();
                break;
        }
    }

    /**
     * Prompts the user for name and phone number input
     * No name validation.
     * Will ask for valid phone number
     * until one is provided by the user
     */
    private void showInsertMenu() {
        String name;
        String phoneNumber;

        outputWriter.write("Input name:\n");

        handleNewPhoneInput();
    }

    /**
     * Reads name and phoneNumber from console
     * and adds to the phone book
     */
    private void handleNewPhoneInput() {

        String name = inputReader.readLine();
        String phoneNumber = readPhoneNumberInput();

        if (phoneBook.addPhoneEntry(phoneNumber, name)) {
            outputWriter.write("Successfully added phone entry\n");
            pressEnterToContinue();
            showMainMenu();
        } else {
            outputWriter.write("There is another phone entry associated with this phone number\n");
            pressEnterToContinue();
            showInsertMenu();
        }
    }

    /**
     * Util method to read input while valid phone number is entered
     *
     * @return Valid {@link String} representation of the
     * phone number without the country code
     */
    private String readPhoneNumberInput() {
        String phoneNumber;
        while (true) {
            outputWriter.write("Input phone number starting with 0 or +359 followed by 9 digits\n");
            phoneNumber = inputReader.readLine();
            //Break if valid phone number is inputed
            if (Validator.isValidPhoneNumber(phoneNumber))
                break;
            else outputWriter.write("Invalid phone number\n");
        }

        //Remove country code from phone number
        return phoneNumber.replaceFirst("\\+359|0", "");
    }

    /**
     * Util method to wait for any input of the user
     */
    private void pressEnterToContinue() {
        outputWriter.write("Press any key to continue\n");
        inputReader.readLine();
    }
}
