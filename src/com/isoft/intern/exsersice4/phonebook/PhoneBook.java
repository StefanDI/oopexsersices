package com.isoft.intern.exsersice4.phonebook;

import java.util.*;

/**
 * @author Stefan Ivanov
 */
public class PhoneBook {

    private Map<String, PhoneEntry> phoneEntriesByPhoneNumber;

    public PhoneBook() {
        phoneEntriesByPhoneNumber = new HashMap<>();
    }

    /**
     * Adds the phone entry by phoneNumber and name
     *
     * @param phoneNumber phoneNumber of the phone entry
     * @param name        name of the phone entry
     * @return false if another phone entry with this phone number exists
     */
    public boolean addPhoneEntry(String phoneNumber, String name) {
        if (phoneEntriesByPhoneNumber.containsKey(phoneNumber))
            return false;

        PhoneEntry entry = new PhoneEntry(phoneNumber, name);
        phoneEntriesByPhoneNumber.put(phoneNumber, entry);
        return true;
    }

    /**
     * Returns Phone Entry associated with phone number
     *
     * @param phoneNumber Phone number to check
     * @return {@link PhoneEntry} with that number, null if no such phone number
     */
    public PhoneEntry getPhoneEntryByPhoneNumber(String phoneNumber) {
        if (!phoneEntriesByPhoneNumber.containsKey(phoneNumber))
            return null;

        return phoneEntriesByPhoneNumber.get(phoneNumber);
    }

    /**
     * Returns all entries with the same name
     * case insensitive
     *
     * @param name name to be matched in phone entries
     * @return {@link List<PhoneEntry>} with all PhoneEntries matching the name
     */
    public List<PhoneEntry> getAllByName(String name) {
        List<PhoneEntry> outputList = new ArrayList<>();
        for (PhoneEntry value : phoneEntriesByPhoneNumber.values()) {
            if (value.name.toLowerCase().equals(name.toLowerCase()))
                outputList.add(value);
        }

        return outputList;
    }

    /**
     * Removes the phone entry associated with this phone number
     *
     * @param phoneNumber phoneNumber to be deleted
     * @return true if success, false if no such phone number
     */
    public boolean removeEntryByPhoneNumber(String phoneNumber) {
        if (!phoneEntriesByPhoneNumber.containsKey(phoneNumber))
            return false;

        phoneEntriesByPhoneNumber.remove(phoneNumber);
        return true;
    }

    /**
     * Phone entry util class holding phoneNumber and name
     */
    public class PhoneEntry {
        private String phoneNumber;
        private String name;

        private PhoneEntry(String phoneNumber, String name) {
            this.phoneNumber = phoneNumber;
            this.name = name;
        }

        @Override
        public String toString() {
            return String.format("Phone number: %s Name: %s", phoneNumber, name);
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PhoneEntry entry = (PhoneEntry) o;
            return phoneNumber.equals(entry.phoneNumber);
        }

        @Override
        public int hashCode() {
            return Objects.hash(phoneNumber);
        }
    }
}
