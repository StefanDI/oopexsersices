package com.isoft.intern.exsersice4;

import com.isoft.intern.exsersice4.menu.Menu;
import com.isoft.intern.io.ConsoleInputReader;
import com.isoft.intern.io.ConsoleOutputWriter;

/**
 * @author Stefan Ivanov
 */
public class Main {
    public static void main(String[] args) {
        Menu menu = new Menu(new ConsoleInputReader(), new ConsoleOutputWriter());
        menu.start();
    }
}
